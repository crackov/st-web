import { gql } from 'apollo-boost';

export default gql`
  query lead($id: ID) {
    lead(_id: $id) {
      __typename
      _id
      kind
      category {
        name
        _id
      }
      phone
      email
      address
      ... on Person {
        givenName
        familyName
        gender
      }
      ... on Company {
        name
        contactPersonName
        website
      }
    }
  }
`;
