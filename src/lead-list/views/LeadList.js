import React from 'react';

import LeadCard from './LeadCard';

export default ({ leads, isLoading, showDelete, onDelete }) => (
  <section className="PageSection">
    <div className="LeadList">
      {isLoading &&
        !leads &&
        Array(3)
          .fill(null)
          .map((el, i) => <LeadCard key={i} isLoading />)}
      {leads &&
        leads.map(lead => (
          <LeadCard key={lead._id} lead={lead} showDelete={showDelete} onDelete={onDelete} />
        ))}
    </div>
  </section>
);
