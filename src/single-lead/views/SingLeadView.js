import React, { Component } from 'react';
import { FormGroup, InputGroup, HTMLSelect, Button, Intent } from '@blueprintjs/core';

const Input = ({ onChange, id, value, placeholder, label }) => {
  return (
    <FormGroup label={label} labelFor={id}>
      <InputGroup id={id} placeholder={placeholder} value={value} onChange={onChange} />
    </FormGroup>
  );
};

const initState = {
  loaded: false,
  id: '',
  lead: {
    _id: '',
    email: '',
    kind: '',
    address: '',
    category: '',
    phone: '',
    givenName: '',
    familyName: '',
    gender: '',
    name: '',
    contactPersonName: '',
    website: '',
  },
};

export default class SingleLeadView extends Component {
  constructor(props, ctx) {
    super(props, ctx);
    this.state = initState;
    this.state.id = props.id;
  }

  static getDerivedStateFromProps(props, state) {
    // Reset form if different id is passed as a route
    if (state.id !== props.id) {
      return Object.assign({}, initState, { id: props.id });
    }
    // Load lead from props
    if (!state.loaded && !props.isLoading && props.lead) {
      const category = props.lead.category && props.lead.category._id;
      const newState = {
        lead: { ...state.lead, ...props.lead, category },
        loaded: true,
      };

      return newState;
    }

    if (!state.loaded && !props.isLoading && !props.id && props.categories) {
      const newState = {
        lead: { ...state.lead, category: props.categories[0]._id },
        loaded: true,
      };

      return newState;
    }
    return state;
  }

  handleChange(e) {
    const lead = Object.assign({}, this.state.lead, { [e.target.id]: e.target.value });
    this.setState({ lead });
  }

  render() {
    const { lead } = this.state;
    const isPerson = lead.kind === 'Person';
    const isCompany = lead.kind === 'Company';
    const title = lead._id ? `Editing Lead: ${lead._id}` : 'Add Lead';

    return (
      <section className="PageSection">
        <div className="LeadList">
          <div className="LeadList-card">
            <h3 className="LoginSection-title">{title}</h3>
            <form onSubmit={e => this.props.onSave(e, this.state.lead)}>
              <FormGroup label="Kind" labelFor="kind">
                <HTMLSelect
                  fill
                  id="kind"
                  value={this.state.lead.kind}
                  onChange={e => this.handleChange(e)}
                >
                  <option>Choose an item...</option>
                  <option value="Person">Person</option>
                  <option value="Company">Company</option>
                </HTMLSelect>
              </FormGroup>
              <Input
                id="email"
                label="Email"
                placeholder="me@example.com"
                value={this.state.lead.email}
                onChange={e => this.handleChange(e)}
              />
              <Input
                id="address"
                label="Address"
                placeholder="St. George Avn. 87"
                value={this.state.lead.address}
                onChange={e => this.handleChange(e)}
              />
              <FormGroup label="Category" labelFor="category">
                <HTMLSelect
                  fill
                  disabled={!this.props.categories}
                  id="category"
                  value={this.state.lead.category}
                  onChange={e => this.handleChange(e)}
                >
                  {this.props.categories &&
                    this.props.categories.map(cat => (
                      <option key={cat._id} value={cat._id}>
                        {cat.name}
                      </option>
                    ))}
                </HTMLSelect>
              </FormGroup>
              <Input
                id="phone"
                label="Phone"
                placeholder="+123888777"
                value={this.state.lead.phone}
                onChange={e => this.handleChange(e)}
              />
              {isPerson && (
                <Input
                  id="givenName"
                  label="Given Name"
                  placeholder="John"
                  value={this.state.lead.givenName}
                  onChange={e => this.handleChange(e)}
                />
              )}
              {isPerson && (
                <Input
                  id="familyName"
                  label="Family Name"
                  placeholder="Doe"
                  value={this.state.lead.familyName}
                  onChange={e => this.handleChange(e)}
                />
              )}
              {isPerson && (
                <FormGroup label="Gender" labelFor="gender">
                  <HTMLSelect
                    fill
                    id="gender"
                    value={this.state.lead.gender}
                    onChange={e => this.handleChange(e)}
                  >
                    <option value="unknown">unknown</option>
                    <option value="male">male</option>
                    <option value="female">female</option>
                  </HTMLSelect>
                </FormGroup>
              )}
              {isCompany && (
                <Input
                  id="name"
                  label="Name"
                  placeholder="Acme Ltd."
                  value={this.state.lead.name}
                  onChange={e => this.handleChange(e)}
                />
              )}
              {isCompany && (
                <Input
                  id="contactPersonName"
                  label="Contact Person Name"
                  placeholder="John"
                  value={this.state.lead.contactPersonName}
                  onChange={e => this.handleChange(e)}
                />
              )}
              {isCompany && (
                <Input
                  id="website"
                  label="Website"
                  placeholder="example.com"
                  value={this.state.lead.website}
                  onChange={e => this.handleChange(e)}
                />
              )}
              <div className="LeadList-actions">
                <Button intent={Intent.SUCCESS} type="submit">
                  Save
                </Button>
                <Button intent={Intent.DANGER} onClick={this.props.onCancel}>
                  Cancel
                </Button>
              </div>
            </form>
          </div>
        </div>
      </section>
    );
  }
}
