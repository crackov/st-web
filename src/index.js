import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';
import { InMemoryCache } from 'apollo-cache-inmemory';

import './index.css';

import App from './App';
import * as serviceWorker from './serviceWorker';
import fragmentLoader from './utils/fragment-loader';

async function main() {
  const fragmentMatcher = await fragmentLoader.load(process.env.REACT_APP_API);

  const cache = new InMemoryCache({
    fragmentMatcher,
    cacheRedirects: {
      Query: {
        lead: ({ leads }, args, { getCacheKey }) => {
          // TODO: Wait for a native way for fragmentMatcher to cache Interface query
          const lead = leads ? leads.find(l => l.id.split(':')[1] === args._id) : {};
          return getCacheKey({ __typename: lead.typename, id: args._id });
        },
      },
    },
  });

  const client = new ApolloClient({
    cache,
    uri: process.env.REACT_APP_API,
    request: op => {
      const token = localStorage.getItem('token');
      op.setContext({
        headers: {
          Authorization: token ? `Bearer ${token}` : null,
        },
      });
    },
  });

  ReactDOM.render(<App apolloClient={client} />, document.getElementById('root'));

  // If you want your app to work offline and load faster, you can change
  // unregister() to register() below. Note this comes with some pitfalls.
  // Learn more about service workers: https://bit.ly/CRA-PWA
  serviceWorker.unregister();
}

main();
