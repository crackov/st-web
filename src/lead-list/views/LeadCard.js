import React from 'react';
import { Link } from 'react-router-dom';
import { SINGLE_LEAD } from '../../routes/routes.const';

import LeadField from './LeadField';

export default ({
  isLoading = false,
  lead: {
    _id,
    email,
    kind,
    address,
    category,
    phone,
    givenName,
    familyName,
    gender,
    name,
    contactPersonName,
    website,
  } = {},
  showDelete,
  onDelete,
}) => {
  const loadingClass = isLoading ? 'bp3-skeleton' : '';
  const isPerson = kind === 'Person';
  const isCompany = kind === 'Company';

  const handleDelete = e => {
    e.preventDefault();
    onDelete(_id);
  };

  return (
    <div className="LeadList-card">
      <div className="LeadList-row">
        {isPerson && <LeadField isLoading={isLoading} label="Given Name" value={givenName} />}
        {isPerson && <LeadField isLoading={isLoading} label="Family Name" value={familyName} />}
        {isPerson && <LeadField isLoading={isLoading} label="Gender" value={gender} />}
        {isCompany && <LeadField isLoading={isLoading} label="Name" value={name} />}
        {isCompany && <LeadField isLoading={isLoading} label="Contact" value={contactPersonName} />}
        {isCompany && <LeadField isLoading={isLoading} label="Website" value={website} />}
        <LeadField isLoading={isLoading} label="Phone" value={phone} />
        <LeadField isLoading={isLoading} label="Email" value={email} />
        <LeadField isLoading={isLoading} label="Address" value={address} />
        <LeadField isLoading={isLoading} label="Kind" value={kind} />
        <LeadField isLoading={isLoading} label="Category" value={category && category.name} />
      </div>
      <div className="LeadList-actions">
        <Link
          to={`${SINGLE_LEAD}/${_id}`}
          className={`bp3-button bp3-intent-primary ${loadingClass}`}
        >
          Edit
        </Link>
        {showDelete && (
          <button className={`bp3-button bp3-intent-danger ${loadingClass}`} onClick={handleDelete}>
            Delete
          </button>
        )}
      </div>
    </div>
  );
};
