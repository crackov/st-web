import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Mutation } from 'react-apollo';

import LOGIN from './gql/login.gql';
import Login from './views/Login';
import { HOME } from '../routes/routes.const';

class LoginPage extends Component {
  static handleSubmit(creds, login) {
    const { username, password } = creds;

    login({ variables: { username, password } }).catch(() => {});
  }

  handleLogin(data) {
    if (!data) return;
    localStorage.setItem('role', data.login.role);
    localStorage.setItem('token', data.login.token);
    this.props.history.push(HOME);
  }

  render() {
    return (
      <Mutation mutation={LOGIN} onCompleted={data => this.handleLogin(data)}>
        {(login, { error, loading }) => {
          return (
            <Login
              onSubmit={creds => LoginPage.handleSubmit(creds, login)}
              error={error}
              loading={loading}
            />
          );
        }}
      </Mutation>
    );
  }
}

export default withRouter(LoginPage);
