import React, { Fragment } from 'react';

export default ({ isLoading = false, label, value }) => {
  const loadingClass = isLoading ? 'bp3-skeleton' : '';

  return (
    <Fragment>
      <span className={`LeadList-label ${loadingClass}`}>{label}</span>
      <span className={`LeadList-item ${loadingClass}`}>{value}</span>
    </Fragment>
  );
};
