import React, { Fragment } from 'react';
import { Navbar, Alignment } from '@blueprintjs/core';

import ActiveLink from './ActiveLink';

export default ({ leftLinks, rightLinks }) => (
  <Fragment>
    <Navbar fixedToTop>
      <Navbar.Group align={Alignment.LEFT}>
        <Navbar.Heading className="hide-on-mobile">SmartTools</Navbar.Heading>
        <Navbar.Divider className="hide-on-mobile" />
        {leftLinks &&
          leftLinks.map((link, i) => (
            <ActiveLink
              key={i}
              to={link.to}
              icon={link.icon}
              text={link.text}
              isPage={link.isPage}
              onClick={link.onClick}
            />
          ))}
      </Navbar.Group>
      <Navbar.Group align={Alignment.RIGHT}>
        {rightLinks &&
          rightLinks.map((link, i) => (
            <ActiveLink
              key={i}
              to={link.to}
              icon={link.icon}
              text={link.text}
              isPage={link.isPage}
              onClick={link.onClick}
            />
          ))}
      </Navbar.Group>
    </Navbar>
    <div className="Navigation-devider" />
  </Fragment>
);
