import React, { useState } from 'react';
import { Intent, Button, Card, FormGroup, InputGroup, Callout } from '@blueprintjs/core';

export default ({ error, onSubmit, loading }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = e => {
    e.preventDefault();
    onSubmit({ username, password });
    setPassword('');
  };

  return (
    <section className="LoginSection">
      <Card className="LoginSection-card">
        <h3 className="LoginSection-title">Credentials Required</h3>
        <form onSubmit={handleSubmit}>
          {error && (
            <Callout intent={Intent.DANGER} style={{ marginBottom: '15px' }}>
              Login attempt unsuccessful
            </Callout>
          )}
          <FormGroup
            label="Username"
            labelFor="username"
            helperText={error && error.graphQLErrors[0].message}
          >
            <InputGroup
              required
              id="username"
              placeholder="john.doe"
              value={username}
              onChange={e => setUsername(e.target.value)}
            />
          </FormGroup>
          <FormGroup label="Password" labelFor="password">
            <InputGroup
              required
              id="password"
              type="password"
              placeholder="pass1234"
              value={password}
              onChange={e => setPassword(e.target.value)}
            />
          </FormGroup>
          <div className="LoginSection-actions">
            <Button type="submit" intent={Intent.PRIMARY} loading={loading}>
              Login
            </Button>
          </div>
        </form>
      </Card>
    </section>
  );
};
