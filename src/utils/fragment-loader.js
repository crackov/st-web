import ApolloClient, { gql } from 'apollo-boost';
import { IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';

/**
 * Load Union and Interface schemas from a server and returns as a fragmentMatcher
 *
 * @param {string} url GraphQL server url
 * @returns {FragmentMatcher}
 */
const load = async url => {
  const tempClient = new ApolloClient({
    uri: url,
  });
  const res = await tempClient.query({
    query: gql`
      {
        __schema {
          types {
            kind
            name
            possibleTypes {
              name
            }
          }
        }
      }
    `,
  });

  // eslint-disable-next-line no-underscore-dangle
  const filteredTypes = res.data.__schema.types.filter(t => t.possibleTypes);
  const fragmentTypes = res.data;
  // eslint-disable-next-line no-underscore-dangle
  fragmentTypes.__schema.types = filteredTypes;

  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData: fragmentTypes,
  });

  return fragmentMatcher;
};

export default { load };
