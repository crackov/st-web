import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';

import App from './App';

it('renders without crashing', () => {
  const client = new ApolloClient();

  const div = document.createElement('div');
  ReactDOM.render(<App apolloClient={client} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
