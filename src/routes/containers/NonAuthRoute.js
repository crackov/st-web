import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export default ({ redirect, ...rest }) => {
  if (localStorage.getItem('role')) {
    return <Redirect to={redirect} />;
  }
  return <Route {...rest} />;
};
