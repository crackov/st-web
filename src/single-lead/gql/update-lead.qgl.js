import { gql } from 'apollo-boost';

export default gql`
  mutation updateLead($input: UpdateLeadInput!) {
    updateLead(input: $input) {
      _id
    }
  }
`;
