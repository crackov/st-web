import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { withApollo } from 'react-apollo';

import { HOME, SINGLE_LEAD } from '../routes/routes.const';

import NavigationView from './views/NavigationView';

class Navigation extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      leftLinks: [
        {
          to: HOME,
          text: 'Home',
          icon: 'home',
          isPage: true,
          requiresAuth: false,
        },
        {
          to: SINGLE_LEAD,
          text: 'New Lead',
          icon: 'add',
          isPage: true,
          requiresAuth: true,
        },
      ],
      rightLinks: [
        {
          text: 'Logout',
          icon: 'log-out',
          isPage: false,
          onClick: e => this.handleLogout(e),
          requiresAuth: true,
        },
      ],
    };
  }

  handleLogout(e) {
    e.preventDefault();
    localStorage.removeItem('role');
    localStorage.removeItem('token');
    this.props.client.cache.reset();
    this.props.history.push(HOME);
  }

  render() {
    const hasAuth = !!localStorage.getItem('role');

    return (
      <NavigationView
        leftLinks={this.state.leftLinks.filter(l => (l.requiresAuth ? hasAuth : true))}
        rightLinks={this.state.rightLinks.filter(l => (l.requiresAuth ? hasAuth : true))}
      />
    );
  }
}

export default withApollo(withRouter(Navigation));
