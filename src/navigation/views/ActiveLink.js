import React from 'react';
import { Icon } from '@blueprintjs/core';
import { NavLink } from 'react-router-dom';

import { HOME } from '../../routes/routes.const';

export default ({ icon, text, to, isPage, onClick }) => {
  return (
    <NavLink
      to={to || HOME}
      className="bp3-button bp3-minimal nav-link"
      activeClassName={isPage ? 'nav-active' : ''}
      onClick={onClick}
      exact
    >
      <Icon icon={icon} />
      <span>{text}</span>
    </NavLink>
  );
};
