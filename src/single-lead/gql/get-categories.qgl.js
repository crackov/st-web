import { gql } from 'apollo-boost';

export default gql`
  query categories {
    categories {
      _id
      name
    }
  }
`;
