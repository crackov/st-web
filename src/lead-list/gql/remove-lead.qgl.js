import { gql } from 'apollo-boost';

export default gql`
  mutation removeLead($id: ID!) {
    removeLead(_id: $id) {
      _id
    }
  }
`;
