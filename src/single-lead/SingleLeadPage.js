import React from 'react';
import { Query, withApollo } from 'react-apollo';

import SingLeadView from './views/SingLeadView';
import GET_SINGLE_LEAD from './gql/get-single-lead.qgl';
import GET_CATEGORIES from './gql/get-categories.qgl';
import UPDATE_LEAD from './gql/update-lead.qgl';
import CREATE_LEAD from './gql/create-lead.qgl';
import { HOME } from '../routes/routes.const';

export default withApollo(({ match, history, client }) => {
  const handleCancel = e => {
    e.preventDefault();
    history.push(HOME);
  };
  const handleSave = async (e, lead) => {
    e.preventDefault();

    // Clean up data before sending mutation
    const input = Object.assign({}, lead);
    // eslint-disable-next-line no-underscore-dangle
    delete input.__typename;
    if (input.gender === '') delete input.gender;
    if (input._id === '') delete input._id;

    await client.mutate({
      mutation: input._id ? UPDATE_LEAD : CREATE_LEAD,
      variables: { input },
    });
    // eslint-disable-next-line no-alert
    alert(lead._id ? `Saved ${lead._id}` : 'Added new lead');
  };

  return (
    // It is prefered to nest queries since 2.1
    // https://www.apollographql.com/docs/react/react-apollo-migration#compose-to-render-composition
    <Query query={GET_SINGLE_LEAD} variables={{ id: match.params.id }}>
      {({ loading: leadLoading, error: leadError, data: leadData }) => {
        const { lead } = leadData;
        // Sub-query so cache can be used for the leads and categories
        return (
          <Query query={GET_CATEGORIES}>
            {({ error: catError, data: catData }) => {
              if (leadError || catError) {
                return <p>Error :(</p>;
              }

              const { categories } = catData;
              return (
                <SingLeadView
                  lead={lead}
                  id={match.params.id}
                  categories={categories}
                  isLoading={leadLoading}
                  onCancel={handleCancel}
                  onSave={handleSave}
                />
              );
            }}
          </Query>
        );
      }}
    </Query>
  );
});
