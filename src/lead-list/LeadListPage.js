import React from 'react';
import { Query, withApollo } from 'react-apollo';

import GET_LEAD_LIST from './gql/get-lead-list.qgl';
import REMOVE_LEAD from './gql/remove-lead.qgl';

import LeadList from './views/LeadList';

export default withApollo(({ client }) => {
  const shouldShowDelete = () => localStorage.getItem('role') === 'manager';

  const deleteLead = async id => {
    // eslint-disable-next-line no-alert
    const shouldDelete = window.confirm('Are you sure you want to delete the lead?');
    if (!shouldDelete) return;

    await client.mutate({
      mutation: REMOVE_LEAD,
      variables: { id },
    });
    // eslint-disable-next-line no-alert
    alert(`Deleted ${id}`);
  };

  return (
    <Query query={GET_LEAD_LIST} pollInterval={1000}>
      {({ loading, error, data }) => {
        if (loading) {
          return <LeadList isLoading />;
        }
        if (error) {
          return <p>Error :(</p>;
        }

        return (
          <LeadList
            leads={data.leads}
            showDelete={shouldShowDelete()}
            onDelete={id => deleteLead(id)}
          />
        );
      }}
    </Query>
  );
});
