import React from 'react';
import { ApolloProvider } from 'react-apollo';

import './App.css';
import 'normalize.css/normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';

import Routes from './routes/Routes';

const App = ({ apolloClient }) => {
  return (
    <ApolloProvider client={apolloClient}>
      <Routes />
    </ApolloProvider>
  );
};

export default App;
