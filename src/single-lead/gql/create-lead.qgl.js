import { gql } from 'apollo-boost';

export default gql`
  mutation addLead($input: AddLeadInput!) {
    addLead(input: $input) {
      _id
    }
  }
`;
