import React from 'react';
import { BrowserRouter as Router, Switch, Redirect } from 'react-router-dom';

import AuthRoute from './containers/AuthRoute';
import NonAuthRoute from './containers/NonAuthRoute';
import Navigation from '../navigation/Navigation';
import LoginPage from '../login/LoginPage';
import LeadListPage from '../lead-list/LeadListPage';
import SingleLeadPage from '../single-lead/SingleLeadPage';

import { AUTH, HOME, SINGLE_LEAD } from './routes.const';

export default () => (
  <Router>
    <Navigation />
    <Switch>
      <AuthRoute path={HOME} redirect={AUTH} exact component={LeadListPage} />
      <NonAuthRoute path={AUTH} redirect={HOME} exact component={LoginPage} />
      <AuthRoute path={SINGLE_LEAD} redirect={AUTH} exact component={SingleLeadPage} />
      <AuthRoute path={`${SINGLE_LEAD}/:id`} redirect={AUTH} exact component={SingleLeadPage} />
      <Redirect to={HOME} />
    </Switch>
  </Router>
);
