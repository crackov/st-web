import { gql } from 'apollo-boost';

export default gql`
  {
    leads {
      __typename
      _id
      kind
      category {
        name
        _id
      }
      phone
      email
      address
      ... on Person {
        givenName
        familyName
        gender
      }
      ... on Company {
        name
        contactPersonName
        website
      }
    }
  }
`;
